package au.edu.uts.aip.flatmate.web;

import au.edu.uts.aip.flatmate.domain.Customer;
import au.edu.uts.aip.flatmate.domain.CustomerHelper;
import au.edu.uts.aip.flatmate.domain.FlatMateBean;
import au.edu.uts.aip.flatmate.domain.SubLevel;
import java.io.Serializable;
import java.util.Date;
import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.component.UIInput;
import javax.faces.context.FacesContext;
import javax.faces.event.ComponentSystemEvent;
import javax.inject.Named;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;

/**
 * account controller for customer login, logout, register, change plan function
 * @author weiming
 */
@Named
@SessionScoped
public class AccountController extends AbstractController implements Serializable {

    @EJB
    private FlatMateBean flatMateBean;

    private String inputId;
    private String inputPassword;
    private Customer currentCustomer = new Customer();
    private boolean logined = false;
    private boolean subscribed = false;

    /**
     * get input id
     * @return inputId
     */
    public String getInputId() {
        return inputId;
    }

    /**
     * set input Id
     * @param inputId
     */
    public void setInputId(String inputId) {
        this.inputId = inputId;
    }

    /**
     * get input password
     * @return
     */
    public String getInputPassword() {
        return inputPassword;
    }

    /**
     * set input password
     * @param inputPassword
     */
    public void setInputPassword(String inputPassword) {
        this.inputPassword = inputPassword;
    }

    /**
     * get current customer
     * @return currentCustomer
     */
    public Customer getCurrentCustomer() {
        return currentCustomer;
    }

    /**
     * set current customer
     * @param currentCustomer
     */
    public void setCurrentCustomer(Customer currentCustomer) {
        this.currentCustomer = currentCustomer;
    }

    /**
     * boolean if login, return true, otherwise, return false
     * @return logined - boolean
     */
    public boolean isLogined() {
        return logined;
    }

    /**
     * set login status
     * @param logined
     */
    public void setLogined(boolean logined) {
        this.logined = logined;
    }

    /**
     * Set a flat to deferentiate subscribed user
     * @return subscribed - boolean
     */
    public boolean isSubscribed() {
        if (currentCustomer.getId() != null) {
            return currentCustomer.getSubLevel() != SubLevel.FREE;
        }
        return subscribed;
    }

    /**
     * Get the flat if customer is subscribed
     * @param customer
     * @return
     */
    public boolean isCustomerSubscribed(Customer customer) {
        if (customer.getId() != null) {
            return customer.getSubLevel() != SubLevel.FREE;
        }
        return false;
    }

    /**
     * set subscribed
     * @param subscribed
     */
    public void setSubscribed(boolean subscribed) {
        this.subscribed = subscribed;
    }

    /**
     * login as customer
     * @return index if login successfully,
     *         null  if username or password is incorrect
     */
    public String login() {
        HttpServletRequest request = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
        try {
            if(flatMateBean.findCustomer(inputId) == null){
                throw new ServletException();
            }
            if (logined != Boolean.TRUE) {
                request.login(inputId, inputPassword);  //use loginform detail to login
            } else {
                request.login(currentCustomer.getId(),currentCustomer.getPassword());   //use registerform detail to login
            }
            currentCustomer = flatMateBean.findCustomer(inputId);
            toggleLoginState();
        } catch (ServletException e) {
            printErrorMessage("Incorrect username or password");
            return null;
        }
        return "index?faces-redirect=true";
    }

    /**
     * for customer logout
     * @return index if logout successfully,
     *         null  if logout unsuccessfully
     */
    public String logout() {
        HttpServletRequest request = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
        try {
            request.logout();
            currentCustomer = new Customer();
            toggleLoginState();
            return "index?faces-redirect=true";
        } catch (ServletException e) {
            printErrorMessage(e.getMessage());
            return null;
        }
    }

    /**
     * register as customer
     * @return index if register successfully
     *         null  if register unsuccessfully
     */
    public String register() {
        if (flatMateBean.findCustomer(currentCustomer.getId()) == null) {
            SubLevel sublvl = currentCustomer.getSubLevel();
            Date date = CustomerHelper.calculateExpiryDate(sublvl);
            int points = CustomerHelper.calculatePointsBasedonSublvl(sublvl);
            String incryptedPassword = CustomerHelper.incryptePassword(currentCustomer.getPassword());

            currentCustomer.setExpiryDate(date);
            currentCustomer.setPoints(points);
            currentCustomer.setPassword(incryptedPassword);

            flatMateBean.createCustomer(currentCustomer);
            return "index?faces-redirect=true";
        } else {
            printErrorMessage("This username has been registered, please change another one.");
        }
        return null;
    }

    /**
     * customer change their plan
     * free, standard, and premium
     * each plan has corresponding points
     */
    public void changePlan() {
        SubLevel sublvl = currentCustomer.getSubLevel();
        Date date = CustomerHelper.calculateExpiryDate(sublvl);
        int points = CustomerHelper.calculatePointsBasedonSublvl(sublvl);

        currentCustomer.setExpiryDate(date);
        currentCustomer.setPoints(points);

        flatMateBean.updateCustomer(currentCustomer);
        printInfoMessage("Your plan has been sucessfully changed!");
    }

    /**
     * Change the account login state when either login or logout
     */
    private void toggleLoginState() {
        logined = logined != Boolean.TRUE;
    }


    /**
     * refer from: http://www.mkyong.com/jsf2/multi-components-validator-in-jsf-2-0/
     * check if password and confirm password are the same
     *
     * @param event 
     */
    public void validatePassword(ComponentSystemEvent event) {

        FacesContext fc = FacesContext.getCurrentInstance();

        UIComponent components = event.getComponent();

        // get password
        UIInput uiInputPassword = (UIInput) components.findComponent("password");
        String password = uiInputPassword.getLocalValue() == null ? ""
                : uiInputPassword.getLocalValue().toString();
        String passwordId = uiInputPassword.getClientId();

        // get confirm password
        UIInput uiInputConfirmPassword = (UIInput) components.findComponent("confirmPassword");
        String confirmPassword = uiInputConfirmPassword.getLocalValue() == null ? ""
                : uiInputConfirmPassword.getLocalValue().toString();

        // Let required="true" do its job.
        if (password.isEmpty() || confirmPassword.isEmpty()) {
            return;
        }

        if (!password.equals(confirmPassword)) {
            FacesMessage msg = new FacesMessage("Password must match confirm password");
            msg.setSeverity(FacesMessage.SEVERITY_ERROR);
            fc.addMessage(passwordId, msg);
            fc.renderResponse();
        }

    }
}
