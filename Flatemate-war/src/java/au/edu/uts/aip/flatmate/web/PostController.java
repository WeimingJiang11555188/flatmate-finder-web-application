package au.edu.uts.aip.flatmate.web;

import au.edu.uts.aip.flatmate.domain.ConcurrentException;
import au.edu.uts.aip.flatmate.domain.Customer;
import au.edu.uts.aip.flatmate.domain.FlatMateBean;
import au.edu.uts.aip.flatmate.domain.Post;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

/**
 * a BackingBean for post(CRUD) views in viewScoped
 * @author weiming
 */
@Named
@ViewScoped
public class PostController extends AbstractController implements Serializable {

    @EJB
    private FlatMateBean flatMateBean;

    private Post currentPost = new Post();
    private Post concurrentModification;
    private List<Post> posts = new ArrayList();

    @Inject
    private AccountController userController;

    /**
     * get a list of posts
     * @return a list of posts
     */
    public List<Post> getPosts() {
        return posts;
    }

    /**
     * an post constructor to initialize the post
     */
    @PostConstruct
    public void init() {
        posts = flatMateBean.findPostsByTopDate();
    }

    /**
     * load the posts ordered by the latest date
     */
    public void loadPostsByLatest() {
        posts = flatMateBean.findPostsByLatest();
    }

    /**
     * retrieve the posts ordered by lowest rent
     */
    public void loadPostsByLowestRent() {
        posts = flatMateBean.findPostsByLowestRent();
    }


    /**
     * load post
     * @param id 
     */
    public void loadPost(int id) {
        currentPost = flatMateBean.findPost(id);
    }

    /**
     * get current post
     * @return currentPost
     */
    public Post getCurrentPost() {
        return currentPost;
    }

    /**
     * get the concurrent modification
     * @return concurrentModification
     */
    public Post getConcurrentModification() {
        return concurrentModification;
    }

    /**
     * set the concurrent modification
     * @param concurrentModification 
     */
    public void setConcurrentModification(Post concurrentModification) {
        this.concurrentModification = concurrentModification;
    }


    /**
     * create a post with automatically setting the date, customer, top date
     * @return myrecord  - turn to my record page
     */
    public String createPost() {
        Date today = new Date();
        currentPost.setDate(today);
        currentPost.setCustomer(userController.getCurrentCustomer());
        currentPost.setTopDate(today);
        flatMateBean.createPost(currentPost);
        return "myrecord?faces-redirect=true";
    }

    /**
     * Updates the post in the database.
     * In the user interface, a concurrent warning is shown when the
     * concurrency exception is caught during updating the post.
     * @return the myrecord.xhtml to show
     *
     * Reference: AIP Week 12 Solution
     */
    public String savePost() {
        try {
            currentPost = flatMateBean.updatePostDetails(currentPost);
            return "myrecord?faces-redirect=true";
        } catch (ConcurrentException ex) {
            concurrentModification = flatMateBean.findPost(currentPost.getId());
            currentPost.setVersion(concurrentModification.getVersion());
            printErrorMessage("This record cannot be updated due to another user is editting at the same time.");
            return null;
        }
    }

    /**
     * delete the post
     * @param id - the id of the post which needs to be deleted
     */
    public void deletePost(int id) {
        flatMateBean.deletePost(id);
    }

    /***
     * Top up the post with the given ID
     * @param id The post id, which is to be topped up
     * @throws ConcurrentException
     */
    public void topPost(int id) throws ConcurrentException {
        Post post = flatMateBean.findPost(id);
        Customer customer = post.getCustomer();

        int points = customer.getPoints();
        if (points > 0) {
            post.setTopDate(new Date());
            customer.setPoints(points - 1);
        } else {
            printErrorMessage("Topup Points is not enough, please upgrade your subscription level.");
        }

        flatMateBean.updatePostDetails(post);
        flatMateBean.updateCustomer(customer);
    }

    /**
     * get all the posts in database
     * @return a list of posts
     */
    public List<Post> getAllPosts() {
        return flatMateBean.findAllPosts();
    }

    /**
     * get the posts are created by current customer
     * @return posts created by current customer
     */
    public List<Post> getMyPosts() {
        return flatMateBean.findPostsByCustomerId(userController.getCurrentCustomer());
    }

}
