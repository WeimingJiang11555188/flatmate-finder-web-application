package au.edu.uts.aip.flatmate.web;

import au.edu.uts.aip.flatmate.domain.*;
import java.util.*;
import javax.ejb.*;
import javax.enterprise.context.*;
import javax.inject.*;

/**
 * flatmate controller
 * @author sunda
 */
@Named
@RequestScoped
public class FlatMateController {

    @EJB
    private FlatMateBean flatMateBean;
    private List<Customer> customers;
    
    /**
     * get "customer" property used in FlatMate 
     * @return a list of customer in flatmate
     * reference from week 10 practice
     */
    public List<Customer> getCustomers(){
        if(customers == null)
            customers = flatMateBean.findAllCustomers();
        return customers;
    }
    
    
}
