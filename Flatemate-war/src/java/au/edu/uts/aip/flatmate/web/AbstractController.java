package au.edu.uts.aip.flatmate.web;

import javax.annotation.Resource;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

/**
 *  This abstract class contains common methods
 *  to be shared by all concrete controllers
 */
public abstract class AbstractController {

    @Resource(name = "restBaseUri")
    String restBaseUri;

    /**
     * Adds a "global" error message to the JSF view.
     *
     * @param message the error message to display to the user
     */
    public void printErrorMessage(String message) {
        FacesContext context = FacesContext.getCurrentInstance();
        context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, message, null));
    }

    /**
     * Adds a "global" information message to the JSF view.
     *
     * @param message the info message to display to the user
     */
    public void printInfoMessage(String message) {
        FacesContext context = FacesContext.getCurrentInstance();
        context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, message, null));
    }

    /***
     *
     * @return the rest uri base which is stored in the web.xml
     */
    public String getRestBaseUri() {
        return restBaseUri;
    }

}
