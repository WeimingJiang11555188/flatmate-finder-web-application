package au.edu.uts.aip.flatmate.domain;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.Id;

/**
 * Administrator class for Admin 
 * Each admin has username and password
 * @author weiming 
 */
@Entity
public class Administrator implements Serializable{
    private String username;
    private String password;

    /**
     * username is primary key
     * get user name
     * @return username
     */
    @Id
    public String getUsername() {
        return username;
    }

    /**
     * set username
     * @param username 
     */
    public void setUsername(String username) {
        this.username = username;
    }

    /**
     * get password
     * @return 
     */
    public String getPassword() {
        return password;
    }

    /**
     * set password
     * @param password 
     */
    public void setPassword(String password) {
        this.password = password;
    }
    
}
