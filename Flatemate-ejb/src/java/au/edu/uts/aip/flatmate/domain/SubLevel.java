package au.edu.uts.aip.flatmate.domain;

/**
 * enum of sublevel
 * customer can have free account, standard account or premium account
 * @author sunda
 */
public enum SubLevel {

    FREE,
    STANDARD,
    PREMIUM,
   
}