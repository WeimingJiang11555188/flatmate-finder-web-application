package au.edu.uts.aip.flatmate.domain;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 * customer class 
 * each customer has id, password, sublevel, expiry date, and points
 * @author sunda
 */
@Entity
@XmlRootElement
public class Customer implements Serializable {

    private String id;
    private String password;
    private SubLevel subLevel;
    private Date expiryDate;
    private int points;

    private List<Post> posts = new ArrayList<>();

    /**
     * get id of the customer
     * primary key of the customer table
     * @return id
     */
    @Id
    @XmlAttribute(name="id")
    public String getId() {
        return id;
    }

    /**
     * set id
     * @param id 
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * get password
     * @return 
     */
    @XmlTransient
    public String getPassword() {
        return password;
    }

    /**
     * set password
     * @param password 
     */
    public void setPassword(String password) {
        this.password = password;
    }

    /**
     * get a list of post
     * @return posts
     */
    @XmlElementWrapper(name = "posts")
    @XmlElement(name = "post")
    @OneToMany(mappedBy = "customer", cascade = CascadeType.ALL)
    public List<Post> getPosts() {
        return posts;
    }

    /**
     * set a list of posts
     * @param posts 
     */
    public void setPosts(List<Post> posts) {
        this.posts = posts;
    }

    /**
     * get customer's sublevel
     * @return sublevel
     */
    @XmlTransient
    @Enumerated(EnumType.STRING)
    public SubLevel getSubLevel() {
        return subLevel;
    }

    /**
     * set customer's sublevel
     * @param subLevel 
     */
    public void setSubLevel(SubLevel subLevel) {
        this.subLevel = subLevel;
    }

    /**
     * get the customer's expiry date
     * @return expiryDate
     */
    @XmlTransient
    @Temporal(javax.persistence.TemporalType.DATE)
    public Date getExpiryDate() {
        return expiryDate;
    }

    /**
     * set customer's expiry date
     * @param expiryDate 
     */
    public void setExpiryDate(Date expiryDate) {
        this.expiryDate = expiryDate;
    }

    /**
     * get points
     * @return the points
     */
    @XmlTransient
    public int getPoints() {
        return points;
    }

    /**
     * set points
     * @param points the points to set
     */
    public void setPoints(int points) {
        this.points = points;
    }

    /**
     * toSting: make current customer's details to String 
     * @return customerStr - the current customer's details
     */
    @Override
    public String toString() {
        String customerStr = "\n\tCurrent customer has properties below:" + "\n";
        if (id != null) {
            customerStr += "\tid is " + id + "\n";
            customerStr += "\tpw is " + password + "\n";
            customerStr += "\tsublevel is " + subLevel + "\n";
            customerStr += "\texpDate is " + expiryDate + "\n";
            customerStr += "\tpoints is " + points + "\n";
            customerStr += "\tnum of posts is " + posts.size() + "\n\n";
        } else {
            customerStr += "\tid == null (null customer)\n";
        }
        return customerStr;
    }

}
