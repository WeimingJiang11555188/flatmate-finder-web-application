package au.edu.uts.aip.flatmate.domain;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * This class is used to specify XML/Json output
 * @author junfeng
 */
@XmlRootElement(name = "Posts")
public class PostList {
    private List<Post> posts = new ArrayList<>();

    /**
     * get post size
     * @return size
     */
    @XmlAttribute()
    public int getSize() {
        int size = 0;
        if (!posts.isEmpty()) {
            size = posts.size();
        }
        return size;
    }

    /**
     * get a list of posts
     * @return posts
     */
    @XmlElement(name = "Post")
    public List<Post> getPosts() {
        return posts;
    }

    /**
     * set a list of posts
     * @param posts 
     */
    public void setPosts(List<Post> posts) {
        this.posts = posts;
    }

}
