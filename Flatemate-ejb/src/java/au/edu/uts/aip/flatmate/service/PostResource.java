package au.edu.uts.aip.flatmate.service;

import au.edu.uts.aip.flatmate.domain.Customer;
import au.edu.uts.aip.flatmate.domain.FlatMateBean;
import au.edu.uts.aip.flatmate.domain.PostList;
import javax.ejb.EJB;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;

/**
 * JAX-RS resource: provide a service to export posts
 * @author weiming
 */
@Path("posts")
public class PostResource {

    @EJB
    private FlatMateBean flatMateBean;

    /**
     * to /rest/posts/all - it will display all the posts existed database
     * @return a list of posts
     */
    @GET
    @Path("/all")
    public PostList allPosts() {
        PostList postList = new PostList();
        postList.setPosts(flatMateBean.findAllPosts());
        return postList;
    }

    /**
     * to /rest/posts/id - it will display the posts 
     *                     that the current customer created in database
     * @param CustomerId
     * @return a list of post
     */
    @GET
    @Path("/{id}")
    public PostList getPostList(@PathParam("id") String CustomerId) {
        Customer customer = flatMateBean.findCustomer(CustomerId);
        PostList postList = new PostList();
        postList.setPosts(flatMateBean.findPostsByCustomerId(customer));
        return postList;
    }
}
