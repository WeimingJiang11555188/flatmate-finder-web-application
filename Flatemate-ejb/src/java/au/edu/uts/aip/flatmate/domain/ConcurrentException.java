package au.edu.uts.aip.flatmate.domain;

import javax.ejb.ApplicationException;

/**
 * This exception is thrown if the user attempts to update a record that has
 * been modified concurrently. In other words, this exception will be thrown if
 * one user reads a record, but another user then updates the record, then that
 * first user then updates the original record.
 * When the exception occurs, the transaction will be rolled back so that no
 * changes are saved to the database.
 *
 * Reference: AIP Week12 Solution
 */
@ApplicationException(rollback = true)
public class ConcurrentException extends Exception {

}
