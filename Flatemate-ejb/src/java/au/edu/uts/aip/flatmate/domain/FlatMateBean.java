package au.edu.uts.aip.flatmate.domain;

import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.OptimisticLockException;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

/**
 * an flatmate that demonstrates the use of JPA
 * all operations in this bean are stateless
 * @author sunda
 */
@Stateless
public class FlatMateBean {

    @PersistenceContext
    private EntityManager em;

    /**
     * find a admin by their username which is the primary key
     * @param name unique primary key for the admin
     * @return the corresponding admin entity
     */
    public Administrator findAdmin(String name) {
        return em.find(Administrator.class, name);
    }

   
    /**
     * retrieves all the customer in the database
     * @return a list of customer in the database
     */
    public List<Customer> findAllCustomers() {
        CriteriaBuilder builder = em.getCriteriaBuilder();
        CriteriaQuery<Customer> query = builder.createQuery(Customer.class);
        return em.createQuery(query).getResultList();
    }

    /**
     * find customer by the primary key - username
     * @param name - username
     * @return the corresponding customer entity
     */
    public Customer findCustomer(String name) {
        return em.find(Customer.class, name);
    }

    /**
     * create a customer in flatmate
     * @param c - customer to create
     */
    public void createCustomer(Customer c) {
        em.persist(c);
    }

    /**
     * update the detail of current customer
     * @param c - the customer needs to be updated info.
     */
    public void updateCustomer(Customer c) {
        em.merge(c);
    }

    /**
     * retrieve all the posts
     * @return a list of posts
     */
    public List<Post> findAllPosts() {
        CriteriaBuilder builder = em.getCriteriaBuilder();
        CriteriaQuery<Post> query = builder.createQuery(Post.class);
        return em.createQuery(query).getResultList();
    }

    /**
     * Retrieves all the post in the database by top date.
     * @return a list of all matching post instances in the database
     */
    public List<Post> findPostsByTopDate() {
        CriteriaBuilder builder = em.getCriteriaBuilder();
        CriteriaQuery<Post> query = builder.createQuery(Post.class);
        Root<Post> p = query.from(Post.class);
        query.select(p);
        query.orderBy(builder.desc(p.get(Post_.topDate)), builder.asc(p.get(Post_.id)));
        return em.createQuery(query).getResultList();
    }

    /**
     * Retrieves all the post in the database by customer
     * @param customer
     * @return a list of all matching post instances in the database
     */
    public List<Post> findPostsByCustomerId(Customer customer) {
        CriteriaBuilder builder = em.getCriteriaBuilder();
        CriteriaQuery<Post> query = builder.createQuery(Post.class);
        Root<Post> from = query.from(Post.class);
        query.where(builder.equal(from.get(Post_.customer), customer));
        query.orderBy(builder.desc(from.get(Post_.topDate)), builder.asc(from.get(Post_.id)));
        return em.createQuery(query).getResultList();
    }

    /**
     * find a post by the primary key - id
     * @param id
     * @return the corresponding post entity
     */
    public Post findPost(int id) {
        return em.find(Post.class, id);
    }

    /**
     * update the detail of current post
     * @param currentPost - a post object contains the id of the post
     *                      to update the details
     * @return merged post 
     * @throws ConcurrentException 
     */
    public Post updatePostDetails(Post currentPost) throws ConcurrentException {
        try {
            Post merged = em.merge(currentPost);
            em.flush();
            return merged;
        } catch (OptimisticLockException ex) {
            throw new ConcurrentException();
        }
    }

    /**
     * delete a post from the flatmate 
     * @param id unique id of the post
     */
    public void deletePost(int id) {
        Post post = em.find(Post.class, id);
        em.remove(post);
    }

    /**
     * create a post in flatmate
     * @param post the post to create
     */
    public void createPost(Post post) {
        em.persist(post);
    }

    /**
     * Retrieves all the post in the database by date.
     * @return a list of all matching post instances in the database
     */
    public List<Post> findPostsByLatest() {
        CriteriaBuilder builder = em.getCriteriaBuilder();
        CriteriaQuery<Post> query = builder.createQuery(Post.class);
        Root<Post> p = query.from(Post.class);
        query.select(p);
        query.orderBy(builder.desc(p.get(Post_.date)));
        return em.createQuery(query).getResultList();
    }

    /**
     * Retrieves all the post in the database by lowest rent.
     * @return a list of all matching post instances in the database
     */
    public List<Post> findPostsByLowestRent() {
        CriteriaBuilder builder = em.getCriteriaBuilder();
        CriteriaQuery<Post> query = builder.createQuery(Post.class);
        Root<Post> p = query.from(Post.class);
        query.select(p);
        query.orderBy(builder.asc(p.get(Post_.rent)));
        return em.createQuery(query).getResultList();
    }
}
