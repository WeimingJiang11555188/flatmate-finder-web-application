--Table Accounts (decommisioned)
-- select * from accounts;
-- delete from accounts;

--Table Customer
select * from customer;
delete from customer;

-- User password is userID in lowercase
INSERT INTO CUSTOMER (ID, EXPIRYDATE, PASSWORD, POINTS, SUBLEVEL) VALUES
('Hen', '2015-11-18', 'd44a08de3981889dae0f7ba3980ea9fd24768279fc4ec2fbbdbcf3bf9fe65c22', 10, 'PREMIUM'),
('Din', '2015-11-19', '7f6f787ea6b6a34c64e7ddf6f8e34f2320104cf1484b4dc17e5df95c7024e5c0', 5, 'PREMIUM'),
('Alex', '2015-11-19', '4135aa9dc1b842a653dea846903ddb95bfb8c5a10c504a7fa16e10bc31d1fdf0', 5, 'STANDARD'),
('Jan', NULL, '6a0ac0fd972c325d6ca5512b67a5e0ad996c4a3e9b59971d125164e6d4db1a1c', 0, 'FREE');

--UPDATE CUSTOMER SET expirydate = '2015-1-1' WHERE ID = 'Hen';

--Table Post
select * from post;
--delete from post;

INSERT INTO POST (ID, CONTACT, "DATE", topdate, DETAIL, EMAIL, POSTCODE, RENT, SUBURB, TITLE, "TYPE", CUSTOMER_ID, VERSION) VALUES
(1000, 'Alex', '2014-11-09', '2014-11-09 00:00:00', '100 North St', 'alex@example.com', '2197', 120, 'Liverpool', 'Test Post by Alex', 'Flat', 'Alex', 1),
(1001, 'Alex', '2015-03-06', '2015-03-06 00:00:00', '8 George St', 'dddd@em', '2010', 275, 'Burwood', 'test post title', 'Unit', 'Alex', 1),
(1002, 'Din', '2015-06-14', '2015-06-14 00:00:00', '80 First Ave', 'dddd@em', '2010', 300, 'Burwood', 'test post title', 'Unit', 'Din', 1),
(1003, 'Hen', '2015-07-10', '2015-07-10 00:00:00', '27 More Rd', 'dddd@em', '2008', 350, 'Ashfield', 'test post title', 'Unit', 'Hen', 1),
(1004, 'Din', '2015-08-03', '2015-08-03 00:00:00', '11 King st', 'dddd@em', '2000', 450, 'Sydney', 'test post title', 'Unit', 'Din', 1),
(1005, 'Hen', '2015-09-07', '2015-09-07 00:00:00', '98 Hume Hwy', 'dddd@em', '2000', 400, 'Sydney', 'test post title', 'Unit', 'Hen', 1),
(1006, 'Din', '2015-09-10', '2015-09-10 00:00:00', '205 Karl St', 'din@example.com', '2163', 100, 'Villawoord', 'Test Post by Din', 'House', 'Din', 1),
(1007, 'Alex', '2015-09-21', '2015-09-21 00:00:00', '20 Broadway Rd', 'dddd@em', '2000', 500, 'Sydney', 'test post title', 'Unit', 'Alex', 1),
(1008, 'Hen', '2015-10-05', '2015-10-05 00:00:00', '12 Broughton St', 'dddd@em', '2020', 280, 'Campsie', 'test post title', 'Unit', 'Hen', 1),
(1009, 'Alex', '2015-10-10', '2015-10-10 00:00:00', '86 Gordon Lane', 'dddd@em', '2197', 150, 'Liverpool', 'test post title', 'Unit', 'Alex', 1);

select *
from post
order by "DATE" desc;

select *
from post
order by rent;

select *
from post
order by topdate desc, id;
--Table Admin
select * from administrator;

insert into administrator values ('admin', 'admin');

-- update incrypted PW from plain 'admin' to SHA-256
update administrator
set password='8c6976e5b5410415bde908bd4dee15dfb167a9c873fc4bb8a81f6f2ab448a918'
where username='admin';

--Realm
--View JDBCREALM_USER;
select * from jdbcrealm_user;
select * from jdbcrealm_group;

-- drop view jdbcrealm_user;
-- drop view jdbcrealm_group;

-- create view jdbcrealm_user (username, password) as
-- select id, password
-- from customer;

-- create view jdbcrealm_group (username, groupname) as
-- select id, 'Users'
-- from customer;

create view jdbcrealm_user (username, password) as
select id, password
from customer
union all
select username, password
from administrator;

create view jdbcrealm_group (username, groupname) as
select id, cast('Users' as VARCHAR(10))
from customer
union all
select username, cast('Admins' as VARCHAR(10))
from administrator;

